package com.aakash.tabtest2.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aakash.tabtest2.R;
import com.aakash.tabtest2.adapter.EventsAdapter;
import com.aakash.tabtest2.models.EventModel;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by jarvis on 2/13/16.
 */
public class AllEvents extends Fragment {

    RecyclerView mRecyclerView;
    EventsAdapter mAdapter;
    ArrayList<EventModel> mEvents = new ArrayList<EventModel>();

    public AllEvents() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.all_events, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final CircularProgressView progressView = (CircularProgressView) getView().findViewById(R.id.progress_view);
        Firebase.setAndroidContext(getActivity());
        Firebase ref = new Firebase("https://redturtle.firebaseio.com/eventrush/events");
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.eventsList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new EventsAdapter(mEvents,getActivity());
        mRecyclerView.setAdapter(new AlphaInAnimationAdapter(mAdapter));
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("aakash", "" + dataSnapshot);
                try {
                    for (DataSnapshot eventSnapshot : dataSnapshot.getChildren()) {
                        EventModel model = eventSnapshot.getValue(EventModel.class);
                        mEvents.add(model);
                        mAdapter.setData(mEvents, getActivity());
                        mAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressView.setVisibility(View.INVISIBLE);


            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.d("aakash", "Error occurred: " + firebaseError.getMessage());
            }
        });

    }
}

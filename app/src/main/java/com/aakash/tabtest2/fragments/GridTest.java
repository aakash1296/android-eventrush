package com.aakash.tabtest2.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.aakash.tabtest2.R;
import com.aakash.tabtest2.adapter.GridViewAdapter;

/**
 * Created by jarvis on 2/24/16.
 */
public class GridTest extends Fragment {
    public GridTest() {
    }

    GridView grid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.grid_test, container, false);
        grid = (GridView) v.findViewById(R.id.grid_view);

        grid.setAdapter(new GridViewAdapter(getActivity()));

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(getActivity(), "" + position,
                        Toast.LENGTH_SHORT).show();
            }
        });



        return v;
    }
}

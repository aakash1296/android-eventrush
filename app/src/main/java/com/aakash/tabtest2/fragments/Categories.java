package com.aakash.tabtest2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.aakash.tabtest2.R;
import com.aakash.tabtest2.activities.FullImageActivity;

/**
 * Created by jarvis on 2/13/16.
 */
public class Categories extends Fragment {
    public Categories() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.letv_test, container, false);

        ImageButton btn = (ImageButton) v.findViewById(R.id.imageButton1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), FullImageActivity.class));
            }
        });
        return v;
    }
}

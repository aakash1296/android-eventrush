package com.aakash.tabtest2.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aakash.tabtest2.R;
import com.aakash.tabtest2.models.EventModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jarvis on 2/13/16.
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    private List<EventModel> mDataSet = new ArrayList<EventModel>();
    private Context mContext;

    public EventsAdapter(List<EventModel> mDataSet, Context mContext) {
        this.mDataSet = mDataSet;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.events_list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        v.setTag(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EventModel event = mDataSet.get(position);
        Log.d("aakash", "Processing..." + event.getEventName() + " " + Integer.toString(position));
        holder.mEventNameContainer.setText(event.getEventName());
        holder.mSocietyNameContainer.setText(event.getSocietyName());
        holder.mCollegeNameContainer.setText(event.getCollegeName());
        holder.mShortDescriptionContainer.setText(event.getShortDescription());
        Uri uri = Uri.parse(event.getImageUrl());

        Glide.with(mContext)
                .load(uri)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.mImageContainer);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void setData(List<EventModel> mDataSet, Context mContext) {
        this.mDataSet = mDataSet;
        this.mContext = mContext;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageContainer;
        public TextView mEventNameContainer;
        public TextView mSocietyNameContainer;
        public TextView mCollegeNameContainer;
        public TextView mShortDescriptionContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            mImageContainer = (ImageView) itemView.findViewById(R.id.imageContainer);
            mEventNameContainer = (TextView) itemView.findViewById(R.id.eventNameContainer);
            mSocietyNameContainer = (TextView) itemView.findViewById(R.id.societyNameContainer);
            mCollegeNameContainer = (TextView) itemView.findViewById(R.id.collegeNameContainer);
            mShortDescriptionContainer = (TextView) itemView.findViewById(R.id.shortDescriptionContainer);
        }
    }
}

package com.aakash.tabtest2.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.aakash.tabtest2.R;

/**
 * Created by jarvis on 2/28/16.
 */
public class GridViewAdapter extends BaseAdapter {
    Context mContext;

    public GridViewAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public Integer[] images = {
            R.drawable.academics_icon,
            R.drawable.commerce_icon,
            R.drawable.dance_icon,
            R.drawable.debating_icon,
            R.drawable.dramatics_icon,
            R.drawable.entrepreneurship_icon
    };

    public class Holder {
        ImageView imageView;
    }


    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;

        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item,parent,false);
        holder.imageView = (ImageView) rowView.findViewById(R.id.imageView);

        holder.imageView.setImageResource(images[position]);
        Log.d("androidTestBitch", "getViewcalled");

        return rowView;
    }
}

package com.aakash.tabtest2.models;

/**
 * Created by jarvis on 2/13/16.
 */
public class EventModel {
    String eventName;
    String collegeName;
    String category;
    String shortDescription;
    String fullDescription;
    String societyName;
    String imageUrl;
    String email;
    String endTime;
    String eventDate;
    String eventLink;
    String eventVenue;
    String mobileNumber;
    String regisTime;
    String startTime;

    public EventModel(String eventName, String collegeName, String category, String shortDescription, String fullDescription, String societyName, String imageUrl) {
        this.eventName = eventName;
        this.collegeName = collegeName;
        this.category = category;
        this.shortDescription = shortDescription;
        this.fullDescription = fullDescription;
        this.societyName = societyName;
        this.imageUrl = imageUrl;
    }

    public EventModel(String eventName, String collegeName, String category, String shortDescription, String fullDescription, String societyName, String imageUrl, String email, String endTime, String eventDate, String eventLink, String eventVenue, String mobileNumber, String regisTime, String startTime) {
        this.eventName = eventName;
        this.collegeName = collegeName;
        this.category = category;
        this.shortDescription = shortDescription;
        this.fullDescription = fullDescription;
        this.societyName = societyName;
        this.imageUrl = imageUrl;
        this.email = email;
        this.endTime = endTime;
        this.eventDate = eventDate;
        this.eventLink = eventLink;
        this.eventVenue = eventVenue;
        this.mobileNumber = mobileNumber;
        this.regisTime = regisTime;
        this.startTime = startTime;
    }

    public EventModel() {
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventLink() {
        return eventLink;
    }

    public void setEventLink(String eventLink) {
        this.eventLink = eventLink;
    }

    public String getEventVenue() {
        return eventVenue;
    }

    public void setEventVenue(String eventVenue) {
        this.eventVenue = eventVenue;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRegisTime() {
        return regisTime;
    }

    public void setRegisTime(String regisTime) {
        this.regisTime = regisTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getSocietyName() {
        return societyName;
    }

    public void setSocietyName(String societyName) {
        this.societyName = societyName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
